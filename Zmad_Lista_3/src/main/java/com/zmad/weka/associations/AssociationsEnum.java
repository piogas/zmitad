package com.zmad.weka.associations;

public enum AssociationsEnum {
	APRIORI, FILTERED_ASSOCIATOR, FP_GROWTH, GENERALIZED_SEQUENTIAL_PATTERNS, PREDICTIVE_APRIORI, TERTIUS;
}
