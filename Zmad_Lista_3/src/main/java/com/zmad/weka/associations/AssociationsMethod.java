package com.zmad.weka.associations;

import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JTextArea;

import weka.associations.Apriori;
import weka.associations.FPGrowth;
import weka.associations.FilteredAssociator;
import weka.associations.GeneralizedSequentialPatterns;
import weka.associations.PredictiveApriori;
import weka.associations.Tertius;
import weka.core.Instances;

public class AssociationsMethod implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(AssociationsMethod.class.getName());

	private static final String NO_IMPLEMENTATION = "Brak implementacji metody :(";

	private Thread t = new Thread(this);

	public static Vector<AssociationsEnum> getAssociationsMethod() {
		Vector<AssociationsEnum> methods = new Vector<AssociationsEnum>();

		for (AssociationsEnum method : AssociationsEnum.values()) {
			methods.addElement(method);
		}
		return methods;
	}

	private static Instances instances;
	private static AssociationsEnum method;
	private static JTextArea textArea;
	private static JLabel imageLabel;
	private static String[] properties;

	public void executeMethod(Instances instances, AssociationsEnum method, JTextArea textArea, JLabel imageLabel,
			String[] property) throws Exception {
		AssociationsMethod.instances = instances;
		AssociationsMethod.method = method;
		AssociationsMethod.textArea = textArea;
		AssociationsMethod.imageLabel = imageLabel;
		AssociationsMethod.properties = property;

		this.t.start();
	}

	private String executeTertius(Instances instances) throws Exception {
		Tertius tertius = new Tertius();
		tertius.setOptions(properties);
		tertius.buildAssociations(instances);
		return tertius.toString();
	}

	private String executePredictiveApriori(Instances instances) throws Exception {
		PredictiveApriori predictiveApriori = new PredictiveApriori();
		predictiveApriori.setOptions(properties);
		predictiveApriori.buildAssociations(instances);
		return predictiveApriori.toString();
	}

	private String executeGeneralizedSequentialPatterns(Instances instances) throws Exception {
		GeneralizedSequentialPatterns generalizedSequentialPatterns = new GeneralizedSequentialPatterns();
		generalizedSequentialPatterns.setOptions(properties);
		generalizedSequentialPatterns.buildAssociations(instances);
		return generalizedSequentialPatterns.toString();
	}

	private String executeFpGrowth(Instances instances) throws Exception {
		FPGrowth fpGrowth = new FPGrowth();
		fpGrowth.setOptions(properties);
		fpGrowth.buildAssociations(instances);
		return fpGrowth.toString();
	}

	private String executeFilteredAssociator(Instances instances) throws Exception {
		FilteredAssociator filteredAssociator = new FilteredAssociator();
		filteredAssociator.setOptions(properties);
		filteredAssociator.buildAssociations(instances);
		return filteredAssociator.toString();
	}

	private String executeApriori(Instances instances) throws Exception {
		Apriori apriori = new Apriori();
		apriori.setOptions(AssociationsMethod.properties);
		apriori.buildAssociations(instances);

		return apriori.toString();
	}

	public static String[] getAssociationOption(AssociationsEnum apriori) {
		switch (apriori) {
		case APRIORI:
			return new Apriori().getOptions();

		case FILTERED_ASSOCIATOR:
			return new FilteredAssociator().getOptions();

		case FP_GROWTH:
			return new FPGrowth().getOptions();

		case GENERALIZED_SEQUENTIAL_PATTERNS:
			return new GeneralizedSequentialPatterns().getOptions();

		case PREDICTIVE_APRIORI:
			return new PredictiveApriori().getOptions();

		case TERTIUS:
			return new Tertius().getOptions();

		default:
			return null;
		}
	}

	@Override
	public void run() {
		AssociationsMethod.imageLabel.setVisible(true);
		LOGGER.info("Uruchomiono algorytm asocjacyjny");
		try {
			switch (AssociationsMethod.method) {
			case APRIORI:
				AssociationsMethod.textArea.setText(this.executeApriori(AssociationsMethod.instances));
				break;

			case FILTERED_ASSOCIATOR:
				AssociationsMethod.textArea.setText(this.executeFilteredAssociator(AssociationsMethod.instances));
				break;
			case FP_GROWTH:
				AssociationsMethod.textArea.setText(this.executeFpGrowth(AssociationsMethod.instances));
				break;

			case GENERALIZED_SEQUENTIAL_PATTERNS:
				AssociationsMethod.textArea
						.setText(this.executeGeneralizedSequentialPatterns(AssociationsMethod.instances));
				break;

			case PREDICTIVE_APRIORI:
				AssociationsMethod.textArea.setText(this.executePredictiveApriori(AssociationsMethod.instances));
				break;

			case TERTIUS:
				AssociationsMethod.textArea.setText(this.executeTertius(AssociationsMethod.instances));
				break;

			default:
				AssociationsMethod.textArea.setText(NO_IMPLEMENTATION);
				break;
			}
		} catch (Exception e) {
			AssociationsMethod.imageLabel.setVisible(false);
			AssociationsMethod.textArea.setText(e.getMessage().substring(e.getMessage().indexOf(":") + 2));
		}
		AssociationsMethod.imageLabel.setVisible(false);
		LOGGER.info("Zakończono algorytm asocjacyjny");

	}
}
