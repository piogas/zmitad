package com.zmad.weka.file;

import java.io.File;
import java.util.logging.Logger;

import javax.swing.JFileChooser;

import com.zmad.weka.inter.Frame;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class FileUtils {

	private static final Logger LOGGER = Logger.getLogger(FileUtils.class.getName());

	private static String currentDir = "C:\\";

	public static File getFileViaFileChooser(Frame frame) {
		JFileChooser fileChooser = new JFileChooser();

		fileChooser.setCurrentDirectory(new File(currentDir));

		LOGGER.info("Uruchomiono fileChooser");
		int result = fileChooser.showOpenDialog(frame);

		if (result == JFileChooser.APPROVE_OPTION) {
			LOGGER.info("Pobrano plik");
			File selectedFile = fileChooser.getSelectedFile();
			currentDir = selectedFile.getParentFile().getAbsolutePath();
			return selectedFile;
		}

		LOGGER.info("Zrezygnowano z pobrania pliku.");
		return null;

	}

	public static Instances getFileInstance(File file) throws Exception {
		String adress = file.getAbsolutePath();
		LOGGER.info("Pobrano scieżkę pliku");

		LOGGER.info("Rozpoczęcie wczytywania pliku z adresu: " + adress);
		DataSource source = new DataSource(adress);
		LOGGER.info("Pobrano plik.");
		return source.getDataSet();
	}
}
