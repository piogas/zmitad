package com.zmad.weka.data;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class DataUtils {

	public static ListCellRenderer<? super String> getCustomListCellRenderer() {
		return new DefaultListCellRenderer() {

			private static final long serialVersionUID = 869291646229117868L;

			@Override
			public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				JLabel listCellRendererComponent = (JLabel) super.getListCellRendererComponent(list, value, index,
						isSelected, cellHasFocus);
				listCellRendererComponent.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));
				return listCellRendererComponent;
			}
		};
	}

	public static DefaultListSelectionModel getCustomListSelectionModel() {
		return new DefaultListSelectionModel() {

			private static final long serialVersionUID = -5193487963802762985L;

			@Override
			public void setSelectionInterval(int index0, int index1) {
				if (super.isSelectedIndex(index0)) {
					super.removeSelectionInterval(index0, index1);
				} else {
					super.addSelectionInterval(index0, index1);
				}
			}
		};
	}
}
