package com.zmad.weka.inter;

import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class FrameUtils {

	private static final Logger LOGGER = Logger.getLogger(FrameUtils.class.getName());

	public static JButton getNewButton(ActionListener actionListener, String text) {
		JButton button = new JButton(text);
		button.addActionListener(actionListener);

		return button;
	}

	public static void showErrorDialog(JFrame frame, String text, String bug) {
		JOptionPane.showMessageDialog(frame, text, bug, JOptionPane.ERROR_MESSAGE);
		LOGGER.info("Wyswietlono komunikat błędu");

	}

	public static JTable setTableColumnWidth(JTable jTable) {

		for (int i = 0; i < jTable.getColumnCount(); i++) {
			jTable.getColumnModel().getColumn(i).setPreferredWidth(jTable.getModel().getColumnName(i).length() * 10);
		}
		return jTable;
	}

}
