package com.zmad.weka.inter;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.zmad.weka.associations.AssociationsEnum;
import com.zmad.weka.associations.AssociationsMethod;
import com.zmad.weka.data.DataUtils;
import com.zmad.weka.file.FileUtils;

import weka.core.Instances;

public class Frame extends JFrame implements ActionListener {

	private static final long serialVersionUID = -5273359193413299495L;

	private static final Logger LOGGER = Logger.getLogger(Frame.class.getName());

	/**
	 * Zmienne statyczne
	 */
	private static final String[] BUTTONS_WEST = { "Wczytaj plik", "Wprowadź dane", "Info", "Zamknij" };
	private static final String[] INFO_LABEL = { "Wczytany plik:", "Instancji:", "Atrybutów:" };
	private static final String[] BUTTONS_FILE_INFO = { "Wyczyść zaznaczenie", "Zaznacz wszystko",
			"Odwróć zaznaczenie" };
	private static final String[] BUTTONS_ASSOCIATION = { "Wykonaj algorytm" };

	// Kluczami są wartości INFO_LABEL
	private Map<String, JLabel> infoLabel = new HashMap<String, JLabel>();

	private JPanel mainPanel = new JPanel();
	private JPanel infoPanel = new JPanel();
	private JTabbedPane centerInfo = new JTabbedPane();
	private JLabel imageLabel = new JLabel();
	private JList<String> listAttr;
	private JTextArea resultTextArea;
	private JTable propertyTable;
	JComboBox<AssociationsEnum> methodComboBox;

	private Instances instances;
	private Instances selectedInstances;

	public Frame() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		super("Projekt 3 - ZMAD");
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("icon/Deer-50.png")));

		LOGGER.info("Uruchamianie aplikacji");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.mainPanel.setLayout(new BorderLayout());
		this.addWestButton();
		this.addNorthInfo();
		this.mainPanel.setVisible(true);
		this.setContentPane(this.mainPanel);
		this.setVisible(true);
		this.setMinimumSize(new Dimension(700, 400));
	}

	public static void main(String args[]) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException {
		new Frame();
	}

	private void addWestButton() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		JPanel panel = new JPanel();
		int gridy = 0;

		for (String buttonText : BUTTONS_WEST) {
			c.gridy = gridy++;
			JButton button = FrameUtils.getNewButton(this, buttonText);
			button.setPreferredSize(new Dimension(120, 66));
			button.setMinimumSize(new Dimension(120, 50));
			buttonPanel.add(button, c);
			c.gridy = gridy++;
			buttonPanel.add(Box.createRigidArea(new Dimension(5, 10)), c);
		}

		panel.add(buttonPanel);
		this.mainPanel.add(panel, BorderLayout.WEST);
	}

	private void addCenterInfo() {

		this.centerInfo.removeAll();
		this.centerInfo.add("Info", this.createInfoJPanel());

		this.centerInfo.add("Asocjacja", this.createAssociateJPanel());

		this.mainPanel.add(this.centerInfo, BorderLayout.CENTER);

		this.updateInfoLabel(this.instances.numAttributes(), this.instances.numInstances());

	}

	/**
	 * Metoda budująca panel zawierający opcję dla asocjacji.
	 * 
	 * @return
	 */
	private JPanel createAssociateJPanel() {

		Box horizontalBox = Box.createHorizontalBox();
		JPanel panel = new JPanel(new BorderLayout());
		JPanel propertyPanel = new JPanel();
		JButton applyButton = FrameUtils.getNewButton(this, BUTTONS_ASSOCIATION[0]);
		this.resultTextArea = new JTextArea();
		this.methodComboBox = new JComboBox<AssociationsEnum>(AssociationsMethod.getAssociationsMethod());
		this.methodComboBox.addActionListener(this);

		this.resultTextArea.setEditable(false);
		this.resultTextArea.setFont(new Font("Arial", Font.ITALIC, 12));
		horizontalBox.add(new JScrollPane(this.resultTextArea));

		horizontalBox.setBorder(BorderFactory.createTitledBorder("Konsola wyjściowa"));

		JPanel methodPanel = new JPanel();
		methodPanel.setLayout(new BoxLayout(methodPanel, BoxLayout.LINE_AXIS));
		methodPanel.add(this.methodComboBox);

		methodPanel.add(applyButton);

		JPanel propertyTablePanel = new JPanel();
		propertyTablePanel.setLayout(new BoxLayout(propertyTablePanel, BoxLayout.LINE_AXIS));

		propertyPanel.setLayout(new BoxLayout(propertyPanel, BoxLayout.PAGE_AXIS));
		this.propertyTable = this
				.createPropertyTable(AssociationsMethod.getAssociationOption(AssociationsEnum.APRIORI));
		this.propertyTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.propertyTable = FrameUtils.setTableColumnWidth(this.propertyTable);
		JScrollPane tableScrollPane = new JScrollPane(this.propertyTable, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		tableScrollPane.setPreferredSize(new Dimension(this.getWidth(), 35));
		propertyTablePanel.add(tableScrollPane);

		propertyPanel.add(methodPanel, BorderLayout.PAGE_START);
		propertyPanel.add(propertyTablePanel, BorderLayout.PAGE_END);
		horizontalBox.setBorder(BorderFactory.createTitledBorder("Konsola wyjściowa"));

		methodPanel.setBorder(BorderFactory.createTitledBorder("Metody asocjacyjne"));
		propertyTablePanel.setBorder(BorderFactory.createTitledBorder("Opcje metody asocjacyjnej"));
		panel.add(horizontalBox);
		panel.add(propertyPanel, BorderLayout.PAGE_START);

		JPanel iconPanel = new JPanel();
		iconPanel.setLayout(new BoxLayout(iconPanel, BoxLayout.LINE_AXIS));
		iconPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
		iconPanel.add(Box.createHorizontalGlue());
		ImageIcon image = new ImageIcon(ClassLoader.getSystemResource("gif/loader.gif"));
		this.imageLabel.setIcon(image);
		iconPanel.add(Box.createRigidArea(this.imageLabel.getPreferredSize()));
		iconPanel.add(this.imageLabel, BorderLayout.PAGE_END);
		this.imageLabel.setVisible(false);
		panel.add(iconPanel, BorderLayout.PAGE_END);

		return panel;
	}

	/**
	 * Metoda budująca panel zawierający informacje o wczytanim pliku.
	 * 
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JPanel createInfoJPanel() {
		Box horizontalBox;
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		JPanel upperPanel = new JPanel(new BorderLayout());

		List<String> listOfAttr = new ArrayList<String>();

		for (int i = 0; i < this.instances.numAttributes(); i++) {
			listOfAttr.add(this.instances.attribute(i).name());
		}

		this.listAttr = new JList(listOfAttr.toArray());
		this.listAttr.setSelectionModel(DataUtils.getCustomListSelectionModel());
		this.listAttr.setCellRenderer(DataUtils.getCustomListCellRenderer());

		JScrollPane listAttrScroll = new JScrollPane(this.listAttr);

		horizontalBox = Box.createHorizontalBox();
		listAttrScroll.setPreferredSize(this.centerInfo.getMaximumSize());
		horizontalBox.add(listAttrScroll);
		horizontalBox.add(Box.createGlue());
		upperPanel.setBorder(BorderFactory.createTitledBorder("Atrybuty"));
		upperPanel.add(horizontalBox);
		panel.add(upperPanel, BorderLayout.CENTER);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
		buttonPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		buttonPane.add(Box.createHorizontalGlue());
		buttonPane.add(FrameUtils.getNewButton(this, BUTTONS_FILE_INFO[1]));
		buttonPane.add(FrameUtils.getNewButton(this, BUTTONS_FILE_INFO[2]));
		buttonPane.add(FrameUtils.getNewButton(this, BUTTONS_FILE_INFO[0]));
		panel.add(buttonPane, BorderLayout.PAGE_END);

		return panel;
	}

	public JTable createPropertyTable(String[] property) {
		String[][] propertyTable = new String[1][];
		propertyTable[0] = property;
		JTable table = new JTable(propertyTable, property);
		table.setTableHeader(null);
		return table;
	}

	private void updateInfoLabel(int numAttributes, int numInstances) {
		this.infoLabel.get(INFO_LABEL[2]).setText(String.valueOf(numAttributes));
		this.infoLabel.get(INFO_LABEL[1]).setText(String.valueOf(numInstances));
	}

	private void addNorthInfo() {
		this.infoPanel.setLayout(new GridLayout(1, (INFO_LABEL.length * 2) + 1));
		for (String labelText : INFO_LABEL) {
			JLabel label = new JLabel(labelText);
			this.infoPanel.add(label);
			this.infoLabel.put(labelText, new JLabel("brak danych"));
			this.infoLabel.get(labelText).setFont(new Font("Arial", Font.ITALIC, 12));
			this.infoPanel.add(this.infoLabel.get(labelText));
		}

		this.infoPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.mainPanel.add(this.infoPanel, BorderLayout.NORTH);
	}

	private Instances getSelectedArrtibutes() {
		int[] indexes = this.listAttr.getSelectedIndices();

		if (indexes.length == 0) {
			this.selectedInstances = this.instances;
		} else {

			Map<Integer, Integer> indexesMap = new HashMap<>();
			for (int i = 0; i < indexes.length; i++) {
				indexesMap.put(indexes[i], indexes[i]);
			}

			this.selectedInstances = new Instances(this.instances);
			for (int i = this.instances.numAttributes() - 1; i > 0; --i) {
				if (!indexesMap.containsKey(i)) {
					this.selectedInstances.deleteAttributeAt(i);
				}
			}
		}
		return this.selectedInstances;
	}

	private String[] getPropertyFromTable() {
		String[] properties = new String[this.propertyTable.getColumnCount()];
		for (int i = 0; i < this.propertyTable.getColumnCount(); i++) {
			properties[i] = this.propertyTable.getColumnName(i);
		}
		return properties;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(BUTTONS_WEST[0])) {
			try {
				File loadFile = FileUtils.getFileViaFileChooser(this);
				if (loadFile != null) {
					this.instances = FileUtils.getFileInstance(loadFile);
					this.addCenterInfo();
					this.infoLabel.get(INFO_LABEL[0]).setText(loadFile.getName());
				}

			} catch (Exception e1) {
				FrameUtils.showErrorDialog(this, "Wybrany plik jest niepoprawny!", "Błąd wczytywania");
				LOGGER.log(Level.WARNING, "Wybrano błędny plik.", e1);
			}

		} else if (e.getActionCommand().equals(BUTTONS_WEST[1])) {
			// TODO wczytywanie ręczne

		} else if (e.getActionCommand().equals(BUTTONS_WEST[3])) {
			LOGGER.info("Zamykanie programu");
			this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));

		} else if (e.getActionCommand().equals(BUTTONS_FILE_INFO[0])) {
			this.listAttr.clearSelection();

		} else if (e.getActionCommand().equals(BUTTONS_FILE_INFO[1])) {
			for (int i = 0; i < this.listAttr.getModel().getSize(); i++) {
				if (!this.listAttr.isSelectedIndex(i)) {
					this.listAttr.setSelectedIndex(i);
				}
			}

		} else if (e.getActionCommand().equals(BUTTONS_FILE_INFO[2])) {
			for (int i = 0; i < this.listAttr.getModel().getSize(); i++) {
				this.listAttr.setSelectedIndex(i);
			}

		} else if (e.getActionCommand().equals(BUTTONS_ASSOCIATION[0])) {
			AssociationsMethod a = new AssociationsMethod();
			try {
				String[] property = this.getPropertyFromTable();
				a.executeMethod(this.getSelectedArrtibutes(), (AssociationsEnum) this.methodComboBox.getSelectedItem(),
						this.resultTextArea, this.imageLabel, property);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (e.getActionCommand().equals("comboBoxChanged")) {
			this.propertyTable.getModel().getColumnName(1);
			this.propertyTable.setModel(this
					.createPropertyTable(AssociationsMethod
							.getAssociationOption((AssociationsEnum) this.methodComboBox.getSelectedItem()))
					.getModel());
			this.propertyTable = FrameUtils.setTableColumnWidth(this.propertyTable);
		}

	}
}
